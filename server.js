const express = require('express')
const bodyParser = require('body-parser')
const http = require('http')
const webpack = require('webpack')
const setupWebpackMiddleware = require('webpack-dev-middleware')
const path = require('path')
const sass = require('node-sass')

const artificialDelay = 2000

const webpackConfig = {
  entry: ['./src/script.js'],
  mode: 'development',
  module: {
    rules: [
      {
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              compact: false
            }
          }
        ]
      }
    ]
  },
  output: {
    path: '/',
    filename: 'script.js',
    devtoolModuleFilenameTemplate: info => info.resourcePath
  },
  devtool: 'source-map'
}

const port = 8080

let formEntries = [
  {
    nickname: 'Joe',
    nicksname: 'Nick I guess'
  },
  {
    nickname: 'Jack',
    nicksname: 'Nick?'
  }
]

const app = express()
// app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const webpackMiddleware = setupWebpackMiddleware(webpack(webpackConfig))
app.use(webpackMiddleware)
app.get('/', (req, res) => {
  res.status(200).sendFile(path.resolve('./src/index.html'))
})
app.use('/bootstrap', express.static('./node_modules/bootstrap/dist'))
app.use('/jquery', express.static('./node_modules/jquery/dist'))
app.get('/style.css', (req, res) => {
  res.status(200)
    .set('Content-Type', 'text/css')
    .send(
      sass.renderSync({
        file: path.resolve('./src/style.scss'),
        outputStyle: 'expanded'
      }).css.toString()
    )
})

app.get('/form-entries', (req, res) => {
  setTimeout(() => {
    res.status(200)
      .set('Content-Type', 'application/json')
      .send(JSON.stringify(formEntries))
  }, artificialDelay)
})

app.post('/submit', (req, res) => {
  setTimeout(() => {
    try {
      console.log(req.body)
      const formData = req.body
      formEntries.push({
        nickname: formData.nickname,
        nicksname: formData.nicksname
      })
      res.status(200).json(formEntries)
    } catch (e) {
      res.status(500).json({ error: e })
    }
  }, artificialDelay)
})
app.post('/submit-page', (req, res) => {
  setTimeout(() => {
    try {
      console.log(req.body)
      const formData = req.body
      formEntries.push({
        nickname: formData.nickname,
        nicksname: formData.nicksname
      })
      res.redirect(req.get('referer'))
      // if (loadPage) {
      //   res.redirect(req.get('referer'))
      // } else {
      //   res.status(200).json(formEntries)
      // }
    } catch (e) {
      res.status(500).json({ error: e })
    }
  }, artificialDelay)
})

const server = http.createServer(app)
app.set('port', port)
server.listen(app.get('port'), () => {
  console.log(`Web server listening on port ${port}`)
})
