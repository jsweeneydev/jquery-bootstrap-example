# jquery-bootstrap-example

Just a proof of concept I made using jQuery and Bootstrap (and Sass).

It's not a good example of maintainable code, but it does show off some of the capabilities of jQuery and Bootstrap. I'll probably add stuff to it over time.



### Setup

```sh
npm install
node .
```

Then just access http://localhost:8080/