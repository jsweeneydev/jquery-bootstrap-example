const $ = window.$

const ids = {
  'result': '#result',
  'result-item': '#result .result-item',
  'submit-progressbar': '#submit-progress',
  'submit-progress': '#submit-progress .progress-bar-striped .progress-bar-animated'
}

const updateItems = entries => $(() => {
  $(ids['result-item']).remove()
  $.each(entries, (i, item) => {
    // This is a very unmaintainable solution, but it is the "jQuery way"
    // Ideally, a templating solution would make more sense
    const el = $(
      '<div>',
      {
        html: [
          $('<div>', { 'class': 'column', text: `Nickname: ${item.nickname}` }),
          $('<div>', { 'class': 'column', text: `Nick's name: ${item.nicksname}` })
        ]
      }
    )
      .addClass(['result-item', 'column'])
    el.appendTo(ids['result'])
  })
})

$.getJSON('/form-entries')
  .done(updateItems)
  .fail(error => console.log(error))

$(() => {
  $('form').submit(function (e) {
    const form = $(this)
    $(ids['submit-progressbar']).addClass('visible')
    $.ajax({
      type: 'POST',
      url: 'submit',
      data: form.serialize(),
      success: updateItems
    })
      .then(() => {
        $(ids['submit-progressbar']).removeClass('visible')
      })
    e.preventDefault()
  })
})
